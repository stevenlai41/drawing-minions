/**
 * @(#)hw9.java
 *
 *
 * @author:��x�s
 * @version 1.00 2015/6/30
 */

import java.awt.*;
import javax.swing.*;

public class hw9 extends JFrame {

	private int[] x1 = {210,210,190,210,210,165};
	private int[] y1 = {495,520,535,545,565,535};
	private int[] x2 = {610,610,630,610,610,655};
	private int[] y2 = {495,520,535,545,565,535};

	public static void main(String[] args)
	{
		new hw9();
	}
	public hw9()
	{
		super("Minion");
		setSize(800,800);
		setVisible(true);
		setBackground(new Color(255,255,255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void paint(Graphics g)
	{
		g.setColor(new Color(255,255,102)); //Yellow
		g.fillArc(210,80,400,200,0,180);
		g.fillRect(210,180,400,400);
		g.fillArc(210,480,400,200,180,180);
		g.fillPolygon(x1,y1,x1.length);
		g.fillPolygon(x2,y2,x2.length);

		g.setColor(new Color(160,160,160)); //Grey
		g.fillOval(235,145,180,180);
		g.fillOval(410,145,180,180);

		g.setColor(new Color(255,255,255)); //White
		g.fillOval(255,165,140,140);
		g.fillOval(430,165,140,140);

		g.setColor(new Color(0,0,0)); //Black
		g.fillOval(300,215,50,50);
		g.fillOval(475,215,50,50);
		g.fillRect(588,230,24,25);
		g.fillRect(210,230,26,25);
		g.fillRect(425,710,135,35);
		g.fillRect(260,710,135,35);
		g.fillArc(248,695,78,30,0,180);
		g.fillArc(494,695,78,30,0,180);
		g.fillArc(243,701,35,45,90,180);
		g.fillArc(543,701,35,45,270,180);
		g.drawArc(250,35,70,100,345,70);
		g.drawArc(450,40,70,100,345,70);
		g.drawArc(350,25,70,100,345,70);

		g.setColor(new Color(255,102,102));//Red
		g.fillArc(280,280,260,180,180,180);

		g.setColor(new Color(255,255,255));//White
		g.fillRoundRect(290,370,40,30,15,15);
		g.fillRoundRect(330,370,40,30,15,15);
		g.fillRoundRect(370,370,40,30,15,15);
		g.fillRoundRect(410,370,40,30,15,15);
		g.fillRoundRect(450,370,40,30,15,15);
		g.fillRoundRect(490,370,40,30,15,15);

		g.setColor(new Color(0,0,204));// Blue
		g.fillRect(210,470,400,25);
		g.fillRect(306,495,206,90);
		g.fillArc(210,480,400,200,180,180);
		g.fillRect(325,650,70,60);
		g.fillRect(425,650,70,60);
	}
}